# produces the plot for the final project after generating the code from our fortran code


# Code by Ryan O'Dell

# AMS 129 Spring Quarter 2018


# To run the code in command line
# enter python3 plotproducer.py
# However the fortran program must be in a directory called fortran


import os
import matplotlib

matplotlib.use('Agg')

import matplotlib.pyplot as plt

import numpy as np

# defines function  that runs the fortran script
# to compute the numerical output and plots it

def plotproducer():
   #first change working directory to directory containing the fortran
   # code and run the make file 
   
   cmd='make'
   cmd2='./main'
   os.chdir( '../fortran' )
   os.system(cmd)
   os.system(cmd2)
   
   #load the text as a numpy array
   data=np.loadtxt('./output.txt')
   p=len(data)
   
   # removes the .o files from our fortran directory
   
   cmd_clean_up='rm -r function.mod function.o Euler_Forward.o main'
   
   #removes what we don't need in the fortran directory 
   os.system(cmd_clean_up)
   
   #change directory back to python directory

   os.chdir('../python')
   # a is  the first column , i.e. the values on the x-axis
   a=data[:,0]
   # b is the second column  , i.e. the output values of our numerical solution
   # each entry  b[i] corresponds to y(a[i])
   b=data[:,1]
   # since numpy arrays operations are vectortized we can just evaluate the analytic solution
   # at the same values on the x-axis to create an analytic solution

   analytic= -np.sqrt(2*np.log((1+a**2))+4   )
   # set up the error calculation
   error=np.zeros(len(analytic))
   # initialize 
   error[0]=abs(analytic[0]-b[0])
   # the sum up the pointwise error  sum  abs ( y(x_(n)) -numerical solution (x_(n)) )

   for i in range(1,len(analytic)):
      error[i]=error[i-1]+abs(analytic[i]-b[i])

   # now the last entry of error is the total sum

   
   final_err=error[len(error)-1]; 
   # now the begin plotting
 
   plt.interactive(False)
   plt.figure(figsize=(10,10) , dpi=80)
   plt.plot(a ,b , 'ro--', label='Numerical')  # plot numerical scheme
   title='Total  Error = '+str(final_err)  # define the title as a string + concatenates strings in python
   plt.plot(a ,analytic , 'b-', label='Analytic') #plot analytic scheme on same fiure
   plt.title(title , {'fontsize':25} )  # sets title
   plt.xlabel('X-Values' , {'fontsize':20} )   # label the x-axis as 
   plt.ylabel('Y(x)', {'fontsize':20})       # labels y-axis 
   plt.grid(True) # sets a grid
   plt.legend(fontsize = 'xx-large')  # sets a legend with large fontsize
   plt.savefig('plotstuff')  # saves figure

   # returns the array of our x-values and numerical solution
   # i.e. basically the output.txt from the fortran routine

   return data 


# then calls our function once so all the code is run

plotproducer() 
